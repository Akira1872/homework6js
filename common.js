let createNewUser = function (){

    let firstName = prompt ('Enter your first name');
    let lastName = prompt ('Enter your last name');
    let birthdayDate = prompt ('Enter the date of birth in the format dd.mm.yyyy');
    let parts = birthdayDate.split(".");
    let birthday = new Date(parts[2], parts[1] - 1, parts[0]);
    
let newUser = {
            _userName: firstName,
            _userSurname: lastName,
            _userBirthday: birthday,
            
            get firstName(){
                return this._userName;
            },
            get lastName(){
                return this._userSurname;
            },

            get birthday(){
                return this._userBirthday;
            },
    
            setFirstName: function(newFirstName){
                this._userName = newFirstName;
            },
    
            setLastName: function(newLastName){
                this._userSurname = newLastName;
            },

            setBirthday: function(newBirthday){
                this._userBirthday = newBirthday;
            },

            getAge: function(){
                const currentDate = new Date();
                const birthDate = new Date(this._userBirthday);
                let ageMilliseconds = currentDate - birthDate;
                let ageYears = Math.floor(ageMilliseconds / (1000 * 60 * 60 * 24 * 365));
                return ageYears;
            },

            getPassword: function () {
                let fullName = this._userName.charAt(0).toUpperCase() + this._userSurname.toLowerCase() + this._userBirthday.getFullYear();
                 return fullName;
            }
        };
        return newUser;
        
    }

    
    let newUser = createNewUser();
    if (newUser !== null) {
    console.log(newUser.getAge());
    console.log(newUser.getPassword());
}